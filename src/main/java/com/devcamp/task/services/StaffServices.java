package com.devcamp.task.services;

import java.util.ArrayList;

import org.springframework.stereotype.Service;

import com.devcamp.task.models.Staff;

@Service
public class StaffServices {

    Staff staff1 = new Staff(1, "Nguyễn Vũ Phước", 34); 
    Staff staff2 = new Staff(2, "Nguyễn Tấn Đạt", 35); 
    Staff staff3 = new Staff(3, "Võ Văn Kiệt", 30); 

    Staff staff4 = new Staff(4, "Lê Minh Tiến", 25); 
    Staff staff5 = new Staff(5, "Bùi Nhật Trường", 27); 
    Staff staff6 = new Staff(6, "Lê Thanh Tùng", 26); 

    Staff staff7 = new Staff(7, "Nguyễn Thị Mỹ Hân", 20); 
    Staff staff8 = new Staff(8, "Nguyễn Ngọc Như Quỳnh", 23); 
    Staff staff9 = new Staff(9, "Nguyễn Thị Bích Ngà", 21); 

    public ArrayList<Staff> departmentDevcamping(){
        ArrayList<Staff> group1 =  new ArrayList<>();
        group1.add(staff1);
        group1.add(staff2);
        group1.add(staff3);
        return group1;
    }
    
    public ArrayList<Staff> departmentMakerting(){
        ArrayList<Staff> group2 =  new ArrayList<>();
        group2.add(staff4);
        group2.add(staff5);
        group2.add(staff6);
        return group2;
    } 

    public ArrayList<Staff> departmentAccounting(){
        ArrayList<Staff> group3 =  new ArrayList<>();
        group3.add(staff7);
        group3.add(staff8);
        group3.add(staff9);
        return group3;
    } 

    public ArrayList<Staff> getAllStaff(){
        ArrayList<Staff> staffs = new ArrayList<>();
        staffs.add(staff1);
        staffs.add(staff2);
        staffs.add(staff3);
        staffs.add(staff4);
        staffs.add(staff5);
        staffs.add(staff6);
        staffs.add(staff7);
        staffs.add(staff8);
        staffs.add(staff9);
        return staffs;
    }

    public ArrayList<Staff> findOldAge(int age){
        ArrayList<Staff> staffAgeOld = new ArrayList<>();
        ArrayList<Staff> staffs = new ArrayList<>();
        staffs.add(staff1);
        staffs.add(staff2);
        staffs.add(staff3);
        staffs.add(staff4);
        staffs.add(staff5);
        staffs.add(staff6);
        staffs.add(staff7);
        staffs.add(staff8);
        staffs.add(staff9);

        for (Staff staff : staffs) {
            if(staff.getAge() > age){
                staffAgeOld.add(staff);
            }
        }
        return staffAgeOld;
    }
}
