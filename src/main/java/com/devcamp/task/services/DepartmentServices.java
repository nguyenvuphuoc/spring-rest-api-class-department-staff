package com.devcamp.task.services;

import java.util.ArrayList;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.devcamp.task.models.Department;

@Service
public class DepartmentServices {
    @Autowired
    StaffServices staffServices;

    Department department1 = new Department(23, "Devcamping", "12/2, Lầu 4, phòng 023");
    Department department2 = new Department(10, "Makerting", "12/2, Lầu 4, phòng 010");
    Department department3 = new Department(20, "Accounting", "12/2, Lầu 4, phòng 020");

    public ArrayList<Department> getAllDepartment(){
        ArrayList<Department> departments = new ArrayList<>();
        department1.setStaffs(staffServices.departmentDevcamping());
        department2.setStaffs(staffServices.departmentMakerting());
        department3.setStaffs(staffServices.departmentAccounting());

        departments.add(department1);
        departments.add(department2);
        departments.add(department3);
        return departments;
    }

    public ArrayList<Department> findIdDepartment(int id){
        ArrayList<Department> departments = new ArrayList<>();
        department1.setStaffs(staffServices.departmentDevcamping());
        department2.setStaffs(staffServices.departmentMakerting());
        department3.setStaffs(staffServices.departmentAccounting());

        departments.add(department1);
        departments.add(department2);
        departments.add(department3);
       
        ArrayList<Department> findDepartments = new ArrayList<>();

        for (Department department : departments) {
            if(department.getId() == id){
                findDepartments.add(department);
            }
        }
        return findDepartments;
    }

    public ArrayList<Department> findAverageAgeDepartments(int averageAge ){
        ArrayList<Department> departments = new ArrayList<>();
        department1.setStaffs(staffServices.departmentDevcamping());
        department2.setStaffs(staffServices.departmentMakerting());
        department3.setStaffs(staffServices.departmentAccounting());

        departments.add(department1);
        departments.add(department2);
        departments.add(department3);
        
        ArrayList<Department> findAverageAge = new ArrayList<>();

        if(department1.getAverageAge(staffServices.departmentDevcamping()) > averageAge){
            findAverageAge.add(department1);
        }
        if(department2.getAverageAge(staffServices.departmentMakerting()) > averageAge){
            findAverageAge.add(department2);
        }
        if(department3.getAverageAge(staffServices.departmentAccounting()) > averageAge){
            findAverageAge.add(department3);
        }
        return findAverageAge;
    }


    
}
