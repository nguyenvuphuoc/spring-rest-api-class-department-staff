package com.devcamp.task.controllers;

import java.util.ArrayList;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.task.models.Department;
import com.devcamp.task.services.DepartmentServices;

@RestController
@CrossOrigin
public class DepartmentControllers {

    @Autowired
    DepartmentServices departmentServices;

    @GetMapping("/department")
    public ArrayList<Department> getAllDepartment(){
        return departmentServices.getAllDepartment();
    }

    @GetMapping("/department/{id}")
    public ArrayList<Department> findIdDepartment(@PathVariable(value = "id") int id){
        return departmentServices.findIdDepartment(id);
    }
    
    @GetMapping("/department/averageAge/{averageAge}")
    public ArrayList<Department> findAverageAge(@PathVariable(value = "averageAge") int averageAge) {
        return departmentServices.findAverageAgeDepartments(averageAge);
    }
}
