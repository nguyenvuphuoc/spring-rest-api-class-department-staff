package com.devcamp.task.controllers;

import java.util.ArrayList;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.task.models.Staff;
import com.devcamp.task.services.StaffServices;

@RestController
@CrossOrigin
public class StaffControllers {
    @Autowired
    StaffServices staffServices;
    @GetMapping("/staffs")
    public ArrayList<Staff> getStaffs(){
        return staffServices.getAllStaff();
    }

    @GetMapping("/staffs/{age}")
    public ArrayList<Staff> findOldAge(@PathVariable(value = "age") int age){
        return staffServices.findOldAge(age);
    }


}
